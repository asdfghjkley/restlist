var packageJson = require('./package.json');
var express = require('express');
var helmet = require('helmet');
var cors = require('cors');
var fallback = require('express-history-api-fallback');
var compression = require('compression');

var port = process.env.PORT || 9000;
var app = express();
var root = getRoot();

app.use(helmet());
app.use(cors());
app.use(compression());
app.use(express.static(root));
app.use(fallback('index.html', { root: root }));


app.listen(port, function () {
    console.log(`${packageJson.name} v${packageJson.version} online on port ${port} - root ${root}`);
});

/////////////////////////

function getRoot() {
    var DEMO_TYPE = process.env.DEMO_TYPE;
    var productionRoot = __dirname + '/public';
    var demoRoot = __dirname + '/build';
    if (DEMO_TYPE == 'local') {
        return `${demoRoot}/local`;
    } else if (DEMO_TYPE == 'dev') {
        return `${demoRoot}/dev`;
    } else if (DEMO_TYPE == 'uat') {
        return `${demoRoot}/uat`;
    } else if (DEMO_TYPE == 'prod') {
        return `${demoRoot}/prod`;
    } else {
        return productionRoot;
    }
}
