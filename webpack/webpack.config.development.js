const path = require('path');
const webpack = require('webpack');

const publicPath = '/';
const contentBase = path.resolve(__dirname + '/../');
const port = 4000;
const host = '0.0.0.0';

module.exports = {
    entry: [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://' + host + ':' + port,
        'webpack/hot/only-dev-server',
        path.resolve(__dirname + '/../src/client.js')
    ],
    devtool: 'inline-source-map',
    output: {
        path: path.resolve(__dirname),
        filename: 'bundle.js',
        publicPath: publicPath,
        sourceMapFilename: 'bundle.map'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ],
    devServer: {
        port,
        host,
        publicPath,
        contentBase,
        open: false,
        hot: true,
        clientLogLevel: 'warning',
        historyApiFallback: true,
        watchContentBase: false,
        disableHostCheck: true,
        noInfo: false,
        stats: {
            assets: false,
            cached: false,
            cachedAssets: false,
            children: false,
            chunks: false,
            chunkModules: false,
            chunkOrigins: false,
            colors: true,
            depth: false,
            entrypoints: false,
            errors: true,
            errorDetails: true,
            hash: true,
            modules: false,
            moduleTrace: false,
            performance: false,
            providedExports: false,
            publicPath: false,
            reasons: false,
            source: false,
            timings: true,
            usedExports: false,
            version: false,
            warnings: true
          }
    }
};