const path = require('path');
const webpack = require('webpack');
const loaders = require('./webpack.loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const packageJSON = require('../package.json');

const htmlTemplatePath = path.resolve(__dirname + '/../index.ejs');

module.exports = {
    module: {
        rules: loaders
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
                'APP_VERSION': JSON.stringify(packageJSON.version),
                'SERVICE_ENV': JSON.stringify(process.env.SERVICE_ENV)
            }
        }),
        new HtmlWebpackPlugin({
            template: htmlTemplatePath,
            chunksSortMode: 'dependency',
            filename: 'index.html',
            config: {},
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true
            }
        })

    ],
    resolve: {
        extensions: ['.jsx', '.js', '.json']
    }
};
