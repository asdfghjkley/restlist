import {
    cyan500, cyan700, pinkA200,
    grey100, grey300, grey400,
    grey500, white, darkBlack,
    fullBlack, orange500
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';

export default {
    spacing: spacing,
    fontFamily: 'Roboto, sans-serif',
    tex: {
        errorColor: orange500
    },
    textField: {
        errorColor: orange500,
        fontSize: '13px'
    },
    menuItem: {
        selectedTextColor: '#d52116'
    },
    zIndex: {
        menu: 1000,
        appBar: 2000,
        drawerOverlay: 1200,
        drawer: 1300,
        dialogOverlay: 1400,
        dialog: 1500,
        layer: 2000,
        popover: 2100,
        snackbar: 2900,
        tooltip: 3000
    },
    palette: {
        primary1Color: '#ca1001',
        primary2Color: cyan700,
        primary3Color: grey400,
        accent1Color: pinkA200,
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: cyan500,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack
    },
    appBar: {
        height: 55
    }
};