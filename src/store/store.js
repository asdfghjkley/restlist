import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { tokenReducer } from './token';
import { navigationReducer, navigationMiddleware } from './navigation';

const enhancer = compose(
	applyMiddleware(thunk),
	navigationMiddleware,
	window.devToolsExtension ? window.devToolsExtension() : f => f
);

const reducer = combineReducers({
	token: tokenReducer,
	router: navigationReducer,
});

export const store = createStore(reducer, enhancer);