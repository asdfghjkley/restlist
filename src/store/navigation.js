import React from 'react';
import { combineReducers, applyMiddleware, compose, createStore } from 'redux';
import { reduxReactRouter, routerStateReducer, } from 'redux-router';
import { createHistory } from 'history';
import { push } from 'redux-router';
import { Route } from 'react-router';
import { App } from '../App';
import { Login } from '../Login';
import { Profile } from '../Profile';
import { NotFound } from '../components';

export const routes = (
    <Route path="/" component={App}>
        <Route path="login" component={Login}/>
        <Route path="profile" component={Profile}/>
        <Route path="*" component={NotFound}/>
    </Route>
);

export const navigationReducer = routerStateReducer;

export const navigationMiddleware = reduxReactRouter({ routes, createHistory });

export const navigationActions = {
    goToUrl: (url) => push(url),
};

export const navigationSelectors = {
    currentUrl: (state) => state.router.location.pathname,
};
