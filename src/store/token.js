import { apiBaseUrl } from '../appConfig';
import { createReducer } from '../utils/createReducer';
import { navigationActions } from './navigation';

const saveToken = (token) => {
    localStorage.setItem('token', token);
};

const loadToken = () => localStorage.getItem('token');

// Reducers

export const tokenReducer = createReducer({
    'set token': (state, { token }) => ({ ...state, token }),
}, loadToken());

// Actions

const setToken = (token) => ({ type: 'set token', token });

const logIn = (_id, password) => (dispatch, getState) => {
    return fetch(`${apiBaseUrl}/api/v1/user/login`, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ _id, password }),
    })
    .then((result) => result.json())
    .then((result) => {
        const token = result.token;
        dispatch(setToken(token));
        saveToken(token);
        dispatch(navigationActions.goToUrl('profile'));
    });
};

const logOut = () => (dispatch, getState) => {
    dispatch(setToken(null));
    saveToken(null);
};

export const tokenActions = { logIn, logOut };

// Selectors

const token = (state) => state.token;

const isAuthenticated = (state) => !!token(state);

export const tokenSelectors = { token, isAuthenticated };
