import 'babel-polyfill';
import './style.css';
import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Provider } from 'react-redux';
import { ReduxRouter } from 'redux-router';
import { MuiThemeProvider } from 'material-ui';
import { store, routes } from './store';
import themeConfig from './themeConfig';
import { getMuiTheme } from 'material-ui/styles';

injectTapEventPlugin();

render((
        <Provider store={store}>
            <div>
                <MuiThemeProvider muiTheme={getMuiTheme(themeConfig)}>
                    <ReduxRouter routes={routes}/>
                </MuiThemeProvider>
            </div>
        </Provider>
    ),
    document.getElementById('root')
);
