import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { App as AppComponent } from './App.component';
import { navigationActions, navigationSelectors, tokenSelectors } from '../store';

export const App = connect(
    (state) => ({
        isAuthenticated: tokenSelectors.isAuthenticated(state),
    }),
    (dispatch) => bindActionCreators({
        goToUrl: navigationActions.goToUrl,
    }, dispatch)
) (AppComponent);