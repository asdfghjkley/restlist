import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from './Link.container';
import logoImage from '../assets/img/title_logo.png';
import commonStyle from '../themeConfig';

const style = {
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    header: {
        width: '100%',
        height: '55px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: commonStyle.palette.primary1Color,
        boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px',
        zIndex: 100
    },
    links: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    logoImage: {
        height: '55px',
        padding: '0 30px',
    },
    content: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        backgroundColor: 'white',
    },
    footer: {
        width: '100%',
        height: '55px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: commonStyle.palette.primary1Color,
        zIndex: 100
    },
};

export class App extends Component {

    render() {
        return (
            <div style={style.container}>
                <div style={style.header}>
                    <div>
                        <img src={logoImage} style={style.logoImage}/>
                    </div>
                    {this.props.isAuthenticated ? (
                        <div style={style.links}>
                            <Link label="VETRINA" url={'url1'}/>
                            <Link label="SCOPRI" url={'url2'}/>
                            <Link label="NOTIFICSHE" url={'url3'}/>
                            <Link label="PROFILO" url={'profile'}/>
                        </div>
                    ) : null}
                </div>
                <div style={style.content}>
                    {this.props.children}
                </div>
                <div style={style.footer}>
                    <Link label="VETRINA" url={'url1'}/>
                    <Link label="COOKIES" url={'url1'}/>
                    <Link label="PRIVACY" url={'url1'}/>
                    <Link label="CONDITIONS" url={'url1'}/>
                </div>
            </div>
        );
    }
}