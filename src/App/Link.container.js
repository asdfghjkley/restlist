import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { navigationActions, navigationSelectors } from '../store';
import { Link as LinkComponent } from './Link.component';

export const Link = connect(
    (state) => ({
        currentUrl: navigationSelectors.currentUrl(state),
    }),
    (dispatch) => bindActionCreators({
        goToUrl: navigationActions.goToUrl,
    }, dispatch)
) (
    ({ url, currentUrl, goToUrl, label }) => <LinkComponent isActive={url === currentUrl} onClick={() => goToUrl(url)} label={label}/>
);
