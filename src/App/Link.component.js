import React, { Component, PropTypes } from 'react';
import commonStyle from '../themeConfig';

const style = {
    link: {
        padding: '10px',
        cursor: 'pointer',
        textDecoration: 'uppercase',
        fontWeight: '800',
        color: 'rgba(255, 255, 255, 0.82)',
    },
    activeLink: {
        padding: '10px',
        cursor: 'pointer',
        textDecoration: 'uppercase',
        fontWeight: '800',
        color: 'rgba(255, 255, 255, 0.82)',
        borderBottom: 'solid 1px rgba(255, 255, 255, 0.82)',
    },
};

export class Link extends Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        isActive: PropTypes.bool,
        onClick: PropTypes.func,
    };

    render() {
        const { onClick, isActive, label } = this.props;
        return (
            <div onClick={onClick} style={isActive ? style.activeLink : style.link}>
                {label}
            </div>
        );
    }
}