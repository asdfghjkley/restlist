const RESTLISTSERVICE_LOCAL = 'http://localhost:3000';
const RESTLISTSERVICE_DEV = 'http://web.staging.service.restlist.it';
const RESTLISTSERVICE_UAT = 'not found';
const RESTLISTSERVICE_PROD = 'https://service.restlist.it';

export const apiBaseUrl = (() => {
    const SERVICE_ENV = process.env.SERVICE_ENV;
    if (SERVICE_ENV == 'local') {
        return RESTLISTSERVICE_LOCAL;
    } else if (SERVICE_ENV == 'dev') {
        return RESTLISTSERVICE_DEV;
    } else if (SERVICE_ENV == 'uat') {
        return RESTLISTSERVICE_UAT;
    } else if (SERVICE_ENV == 'prod') {
        return RESTLISTSERVICE_PROD;
    }

    throw new Error(`Invalid RESTLISTSERVICE - ${SERVICE_ENV}`);
})();

const URL_APPLE_APP_DOWNLOAD = '#';
const URL_ANDROID_APP_DOWNLOAD = '#';

export const appUrlDownload = {
    apple: URL_APPLE_APP_DOWNLOAD,
    android: URL_ANDROID_APP_DOWNLOAD
};