import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Avatar, ProductCard } from '../components';
import { RaisedButton, FlatButton, TextField, FontIcon } from 'material-ui';
import commonStyle from '../themeConfig';

const style = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '60px 0',
    },
    profileCard: {
        width: '650px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'start',
        padding: '20px',
        border: 'solid 2px #6b6b6b',
        boxShadow: 'rgb(136, 136, 136) 0 0 18px',
        borderRadius: '7px',
    },
    profileCardText: {
        alignItems: 'stretch',
    },
    profileCardRow: {
        padding: '10px 40px',
    },
    avatar: {
        width: '250px',
        margin: '25px 0',
    },
    number: {
        fontSize: '15px',
        fontWeight: 'bold',
    },
    lablel: {
        fontSize: '10px',
        fontWeight: 'bold',
    },
    tabs: {
        marginTop: '30px',
        marginBottom: '30px',
    },
    tab: {
        color: commonStyle.palette.primary1Color,
        fontSize: '20px',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        width: '290px',
        textAlign: 'center',
        paddingBottom: '5px',
    },
    activeTab: {
        color: commonStyle.palette.primary1Color,
        fontSize: '20px',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        borderBottom: `3px solid ${commonStyle.palette.primary1Color}`,
        width: '290px',
        textAlign: 'center',
        paddingBottom: '5px',
    },
};

const imageUrl = 'https://www.goodfood.com.au/content/dam/images/h/0/f/a/q/i/image.related.wideLandscape.940x529.h0fa4n.png/1515456591895.jpg';

export class Profile extends Component {

    static propTypes = {

    };

    render() {
        return (
            <div style={style.container}>
                <div style={style.profileCard}>
                    <Avatar size={140} src='https://www.fillmurray.com/300/300' />
                    <div className="flex column" style={style.profileCardText}>
                        <div className="row" style={style.profileCardRow}>
                            <div className="flex">DIEGO DI BON</div>
                            <FontIcon className="material-icons">settings</FontIcon>
                        </div>
                        <div className="row" style={style.profileCardRow}>
                            diegodibon
                        </div>
                        <div className="row" style={style.profileCardRow}>
                            <div className="flex column">
                                <div style={style.number}>1</div>
                                <div style={style.lablel}>Recensioni</div>
                            </div>
                            <div className="flex column">
                                <div style={style.number}>8</div>
                                <div style={style.lablel}>Foto</div>
                            </div>
                            <div className="flex column">
                                <div style={style.number}>29</div>
                                <div style={style.lablel}>Preferentii</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={style.tabs} className="row">
                    <div style={style.activeTab}>Recensioni</div>
                    <div style={style.tab}>Foto</div>
                    <div style={style.tab}>Preferentii</div>
                </div>
                <div className="row">
                    <ProductCard image={imageUrl}/>
                    <ProductCard image={imageUrl}/>
                    <ProductCard image={imageUrl}/>
                    <ProductCard image={imageUrl}/>
                </div>
            </div>
        );
    }
}