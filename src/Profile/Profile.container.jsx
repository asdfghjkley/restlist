import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Profile as ProfileComponent } from './Profile.component';

export const Profile = connect(
    (state) => ({

    }),
    (dispatch) => bindActionCreators({

    }, dispatch)
) (ProfileComponent);