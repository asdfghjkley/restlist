import React, { Component, PropTypes } from 'react';
import { Dialog, FlatButton } from 'material-ui';

const propTypes = {
    strings: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    setUserAcceptSms: PropTypes.func.isRequired,
    setView: PropTypes.func.isRequired,
    openCloseDialogSmsConfirmation: PropTypes.func.isRequired,
    getPhoneNumberToken: PropTypes.func.isRequired,
    getPhoneNumberTokenRequestCancel: PropTypes.func.isRequired
};
const style = {
    wrapper: {

    }
};

export default class DialogSmsConfirmation extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
    }

    handleClose = () => {
        const { setUserAcceptSms, openCloseDialogSmsConfirmation } = this.props;
        setUserAcceptSms(false);
        openCloseDialogSmsConfirmation(false);
    }

    async handleAccept() {
        const { setUserAcceptSms, openCloseDialogSmsConfirmation, setView, getPhoneNumberToken, getPhoneNumberTokenRequestCancel } = this.props;
        try {
            openCloseDialogSmsConfirmation(false);
            setView(2); //setView to loading
            getPhoneNumberTokenRequestCancel();
            await getPhoneNumberToken();
            setUserAcceptSms(true);
            setView(1);
        } catch (err) {
            console.error(err);
            setView(0);
            openCloseDialogSmsConfirmation(false);
        }
    }

    getAction = () => {
        const { strings } = this.props;
        return (
            <div>
                <FlatButton
                    label={strings.actionButtons.cancell.label}
                    primary={true}
                    onTouchTap={this.handleClose}
                />
                <FlatButton
                    label={strings.actionButtons.continue.label}
                    primary={true}
                    onTouchTap={this.handleAccept.bind(this)}
                    keyboardFocused={true}
                />
            </div>
        );
    }

    render() {
        const { open, strings } = this.props;
        return (
            <div style={style.wrapper}>
                <Dialog
                    title={strings.title}
                    actions={this.getAction()}
                    modal={true}
                    open={open}
                >
                    {strings.description}
                </Dialog>
            </div>
        );
    }
}