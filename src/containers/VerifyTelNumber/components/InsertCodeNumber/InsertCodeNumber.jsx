import React, { Component, PropTypes } from 'react';
import ReactCodeInput from 'react-code-input';
import { FlatButton } from 'material-ui';
import themeConfig from './../../../../themeConfig';

const propTypes = {
    strings: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    numberVerificationResetState: PropTypes.func.isRequired,
    setPhonenumberCode: PropTypes.func.isRequired,
    setView: PropTypes.func.isRequired,
    getPhoneNumberToken: PropTypes.func.isRequired,
    getPhoneNumberTokenRequestCancel: PropTypes.func.isRequired
};
const style = {
    wrapper: {

    },
    description: {
        width: '100%',
        textAlign: 'center',
        paddingBottom: '20px'
    },
    wrapperAction: {
        paddingTop: '5px'
        // paddingBottom: '15px'
    },
    inputNumber: {
        width: '40px',
        margin: '4px',
        fontSize: '30px',
        height: '45px',
        textAlign: 'center',
        borderRadius: '0px',
        boxSizing: 'border-box',
        padding: '0px',
        border: 'none',
        outline: 'none',
        borderBottom: `1px solid ${themeConfig.palette.primary1Color}`
    }
};

export default class InsertCodeNumber extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
    }

    handleOnChangeCodeInput = (value) => {
        const { setPhonenumberCode } = this.props;
        setPhonenumberCode(value);
    }

    handlerOnClickChangePhoneNumber = () => {
        const { numberVerificationResetState } = this.props;
        numberVerificationResetState();
    }

    async handlerOnClickSendBackCode() {
        const { setView, getPhoneNumberToken, getPhoneNumberTokenRequestCancel } = this.props;

        try {
            setView(2); //setView to loading
            getPhoneNumberTokenRequestCancel();
            await getPhoneNumberToken();
            setView(1);
        } catch (err) {
            console.error(err);
            setView(1);
        }
    }

    render() {
        const { strings, data } = this.props;

        return (
            <div style={style.wrapper}>
                <div style={style.description}>
                    {`${strings.views.index1.description} (${data.phoneNumber})`}
                </div>
                <ReactCodeInput
                    inputStyle={style.inputNumber}
                    onChange={this.handleOnChangeCodeInput}
                    fields={4}
                    type='tel'
                />
                <div style={style.wrapperAction}>
                    <div>
                        <FlatButton
                            label={strings.views.index1.actionButtons.changeNumber.label}
                            fullWidth={false}
                            primary={true}
                            onClick={this.handlerOnClickChangePhoneNumber}
                        />
                        <FlatButton
                            label={strings.views.index1.actionButtons.sendBackCode.label}
                            fullWidth={false}
                            primary={true}
                            onClick={this.handlerOnClickSendBackCode.bind(this)}
                        />
                    </div>
                </div>
            </div>
        );
    }
}