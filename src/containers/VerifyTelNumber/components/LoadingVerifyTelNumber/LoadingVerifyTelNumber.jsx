import React, { Component, PropTypes } from 'react';
import { CircularProgress } from 'material-ui';

const propTypes = {
    strings: PropTypes.object.isRequired,
    ui: PropTypes.object.isRequired
};
const style = {
    description: {
        width: '100%',
        textAlign: 'center',
        paddingBottom: '15px'
    }
};

export default class LoadingVerifyTelNumber extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
    }

    getDescription = () => {
        const { strings, ui } = this.props;

        if (ui.isPhoneNumberTokenRequest) return strings.description.sendSms;
        if (ui.isPhoneNumberVerificationRequest) return strings.description.verificationCode;

        return 'error';
    }

    render() {
        return (
            <div>
                <div style={style.description}>
                    {this.getDescription()}
                </div>
                <CircularProgress />
            </div>
        );
    }
}