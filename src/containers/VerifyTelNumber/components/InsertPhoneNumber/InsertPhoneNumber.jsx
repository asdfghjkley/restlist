import React, { Component, PropTypes } from 'react';
import FieldPhone, { formatPhoneNumber, parsePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input';

const propTypes = {
    strings: PropTypes.object.isRequired,
    setIsvalidNumber: PropTypes.func.isRequired,
    setPhonenumber: PropTypes.func.isRequired,
    setPhonenumberCountry: PropTypes.func.isRequired,
    dialogSmsConfirmation: PropTypes.element.isRequired,
    numberVerificationResetState: PropTypes.func.isRequired
};
const style = {
    wrapper: {

    },
    fieldPhone: {
        width: '250px',
        margin: 'auto',
        fontSize: '19px'
    },
    description: {
        width: '100%',
        textAlign: 'center',
        paddingBottom: '20px'
    }
};

export default class InsertPhoneNumber extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { numberVerificationResetState } = this.props;
        numberVerificationResetState(true); //true keepError see action more info
    }

    handleOnChange = (value) => {
        this.updatePhoneNumberData(value);
    }

    updatePhoneNumberData = (value) => {
        const { setIsvalidNumber, setPhonenumber, setPhonenumberCountry } = this.props;

        const fullPhoneNumber = formatPhoneNumber(parsePhoneNumber(value), 'International');
        const isValidNumber = isValidPhoneNumber(fullPhoneNumber);
        const { country, phone } = parsePhoneNumber(fullPhoneNumber);

        setIsvalidNumber({ isValidPhoneNumber: isValidNumber });
        setPhonenumber({ phoneNumber: phone });
        setPhonenumberCountry({ phoneNumberCountry: country });
    }

    render() {
        const { strings, dialogSmsConfirmation } = this.props;
        return (
            <div style={style.wrapper}>
                <div>
                    <div style={style.description}>
                        {strings.views.index0.description}
                    </div>
                    <FieldPhone
                        autoFocus
                        style={style.fieldPhone}
                        country={strings.views.index0.field.phone.defaultCountry}
                        placeholder={strings.views.index0.field.phone.placeholder}
                        onChange={this.handleOnChange}
                    />
                    {dialogSmsConfirmation}
                </div>
            </div>
        );
    }
}