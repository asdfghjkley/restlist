import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
//components
import ServerErrorMessage from './../../components/ServerErrorMessage/ServerErrorMessage';
import DialogSmsConfirmation from './components/DialogSmsConfirmation/DialogSmsConfirmation';
import InsertPhoneNumber from './components/InsertPhoneNumber/InsertPhoneNumber';
import InsertCodeNumber from './components/InsertCodeNumber/InsertCodeNumber';
import LoadingVerifyTelNumber from './components/LoadingVerifyTelNumber/LoadingVerifyTelNumber';

//modules
import {
    resetState as numberVerificationResetState,
    finish as numberVerificationFinish,
    setIsvalidNumber,
    setPhonenumber,
    setPhonenumberCountry,
    setUserAcceptSms,
    openCloseDialogSmsConfirmation,
    setView,
    getPhoneNumberToken,
    getPhoneNumberTokenRequestCancel,
    setPhonenumberCode,
    cleanError
} from './../../store/modules/numberVerification';
//css
import 'react-phone-number-input/rrui.css'; //controllare autoprefixer nel loader css per le performance, guarda la doc del plugin react-phone-number-input
import 'react-phone-number-input/style.css';

const propTypes = {
    strings: PropTypes.object.isRequired,
    ui: PropTypes.object.isRequired,
    setIsvalidNumber: PropTypes.func.isRequired,
    setPhonenumber: PropTypes.func.isRequired,
    setPhonenumberCountry: PropTypes.func.isRequired,
    openCloseDialogSmsConfirmation: PropTypes.func.isRequired,
    setUserAcceptSms: PropTypes.func.isRequired,
    numberVerificationResetState: PropTypes.func.isRequired,
    numberVerificationFinish: PropTypes.func.isRequired,
    setView: PropTypes.func.isRequired,
    getPhoneNumberToken: PropTypes.func.isRequired,
    getPhoneNumberTokenRequestCancel: PropTypes.func.isRequired,
    setPhonenumberCode: PropTypes.func.isRequired,
    cleanError: PropTypes.func.isRequired
};
const style = {
    wrapper: {
        margin: 'auto',
        textAlign: 'center'
    }
};

@connect(
    ({ numberVerification }) => ({
        strings: numberVerification.strings,
        ui: numberVerification.ui,
        data: numberVerification.data
    }),
    {
        numberVerificationResetState,
        numberVerificationFinish,
        setIsvalidNumber,
        setPhonenumber,
        setPhonenumberCountry,
        setUserAcceptSms,
        openCloseDialogSmsConfirmation,
        setView,
        getPhoneNumberToken,
        getPhoneNumberTokenRequestCancel,
        setPhonenumberCode,
        cleanError
    }
)
export default class VerifyTelNumber extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { numberVerificationResetState } = this.props;
        numberVerificationResetState();
    }

    componentWillUnmount() {
        const { numberVerificationFinish } = this.props;
        numberVerificationFinish();
    }

    getView = () => {
        const {
            strings,
            ui,
            setUserAcceptSms,
            openCloseDialogSmsConfirmation,
            setView,
            numberVerificationResetState,
            getPhoneNumberToken,
            getPhoneNumberTokenRequestCancel
        } = this.props;

        switch (ui.viewIndex) {
            case 0:
                return (
                    <InsertPhoneNumber
                        {...this.props}
                        dialogSmsConfirmation={
                            < DialogSmsConfirmation
                                strings={strings.dialogSmsConfirmation}
                                setView={setView}
                                numberVerificationResetState={numberVerificationResetState}
                                openCloseDialogSmsConfirmation={openCloseDialogSmsConfirmation}
                                setUserAcceptSms={setUserAcceptSms}
                                getPhoneNumberToken={getPhoneNumberToken}
                                getPhoneNumberTokenRequestCancel={getPhoneNumberTokenRequestCancel}
                                open={ui.isOpenDialogSmsConfirmation}
                            />
                        }
                    />
                );
            case 1:
                return (
                    <InsertCodeNumber {...this.props} />
                );
            case 2:
                return (
                    <LoadingVerifyTelNumber
                        strings={strings.views.index2}
                        ui={ui}
                    />
                );
            default:
                return 'error';
        }
    }

    render() {
        const { data, cleanError } = this.props;

        return (
            <div style={style.wrapper}>
                {this.getView()}
                <ServerErrorMessage onClose={cleanError} errorMessage={data.error.message} />
            </div>
        );
    }
}