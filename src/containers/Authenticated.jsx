import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'redux-router';

const propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    loginPath: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired
};


export function requireAuthentication(ComponentProtected) {

    class Authenticated extends Component {
        static PropTypes = propTypes;

        componentWillMount() {
            this.checkAuth(this.props.isAuthenticated);
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth(nextProps.isAuthenticated);
        }

        checkAuth(isAuthenticated) {
            if (!isAuthenticated) {
                let redirectAfterLogin = this.props.location.pathname;
                this.props.push(`${this.props.loginPath}?next=${redirectAfterLogin}`);
            }
        }

        render() {
            return (
                <div>
                    {this.props.isAuthenticated === true
                        ? <ComponentProtected {...this.props} />
                        : null
                    }
                </div>
            );

        }
    }

    return connect(
        ({ appSettings, user }) => ({
            isAuthenticated: user.auth.isAuthenticated,
            loginPath: appSettings.routerConfig.login.path
        }),
        { push }
    )(Authenticated);
}
