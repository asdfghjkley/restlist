import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Login as LoginComponent } from './Login.component';
import { tokenActions, tokenSelectors } from '../store';

export const Login = connect(
    (state) => ({

    }),
    (dispatch) => bindActionCreators({
        logIn: tokenActions.logIn,
    }, dispatch)
) (LoginComponent);