import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Paper, InstallAppLogo } from '../components';
import { RaisedButton, FlatButton, TextField } from 'material-ui';
import loginMessageImage from '../assets/img/login_msg.png';
import phoneImage from '../assets/img/phone_login.png';

const style = {
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'start',
        padding: '120px 0',
    },
    phoneImage: {
        height: '400px',
        marginRight: '125px',
    },
    form: {
        width: '300px',
        padding: '20px',
        border: 'solid 2px #6b6b6b',
        boxShadow: 'rgb(136, 136, 136) 5px 10px 18px',
        borderRadius: '7px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginMessageImage: {
        width: '250px',
        margin: '25px 0',
    },
    textField: {
        width: '250px',
        minWidth: '250px',
        backgroundColor: 'white !important',
        margin: '10px 0',
    },
    hintRight: {
        width: '250px',
        color: 'darkgrey',
        fontSize: '12px',
        textAlign: 'right',
        alignSelf: 'flex-end',
        padding: '15px 35px 15px 15px',
    },
    submitButton: {
        width: '250px',
        minWidth: '250px',
    },
    registerButton: {
        textTransform: 'none',
    },
    appsLinks: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: '30px',
    },
};

export class Login extends Component {
    static propTypes = {

    };

    state = {
        username: '',
        password: '',
    };

    handleUsername = (event) => {
        this.setState({ username: event.target.value });
    };

    handlePassword = (event) => {
        this.setState({ password: event.target.value });
    };

    handleSubmit = () => {
        this.props.logIn(this.state.username, this.state.password);
    };

    render() {
        return (
            <div style={style.container}>
                <img src={phoneImage} style={style.phoneImage}/>
                <div>
                    <Paper style={style.form}>
                        <img src={loginMessageImage} style={style.loginMessageImage}/>
                        <TextField value={this.state.username} onChange={this.handleUsername} type="text" placeholder="username" style={style.textField} className="no-autofill" name="username"/>
                        <TextField value={this.state.password} onChange={this.handlePassword} type="password" placeholder="password" style={style.textField} className="no-autofill" name="password"/>
                        <div className="hint" style={style.hintRight}>
                            Hai smarrito la password?
                        </div>
                        <RaisedButton onClick={this.handleSubmit} label="accedi" primary={true} fullWidth={true} style={style.submitButton}/>
                        <p className="hint">
                            Non hai un account?
                            <FlatButton primary={true} label="Registrati" labelStyle={style.registerButton}/>
                        </p>
                    </Paper>
                    <div style={style.appsLinks}>
                        <InstallAppLogo system={'apple'} link={'#'}/>
                        <InstallAppLogo system={'android'} link={'#'}/>
                    </div>
                </div>
            </div>
        );
    }
}