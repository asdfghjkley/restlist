import React, { Component, PropTypes } from 'react';
import { FlatButton, FontIcon } from 'material-ui';
import { AutoRotatingCarousel, Slide } from 'material-auto-rotating-carousel';
import { green400, green600, blue400, blue600, red400, red600 } from 'material-ui/styles/colors';
// import themeConfig from './../../themeConfig';

const propTypes = {
    featuresType: PropTypes.string.isRequired
};
// const style = {};
export default class SliderInfo extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);

        this.state = {
            openFeatures: false
        };
    }

    open = () => {
        this.setState({
            openFeatures: true
        });
    }
    close = () => {
        this.setState({
            openFeatures: false
        });
    }

    render() {
        const { featuresType } = this.props;
        return (
            <div>
                <FlatButton
                    label={`Info ${featuresType}`}
                    icon={<FontIcon className="material-icons">slideshow</FontIcon>}
                    primary={true}
                    onClick={this.open}
                />
                <AutoRotatingCarousel
                    label="Close"
                    onStart={this.close}
                    onRequestClose={this.close}
                    open={this.state.openFeatures}
                    mobile={document.body.clientWidth < 800 ? true : false}
                    autoplay={false}
                >
                    <Slide
                        media={<img src="./8e550c9cc406babdc056ff8098f5ba57.png" />}
                        mediaBackgroundStyle={{ backgroundColor: red400 }}
                        contentStyle={{ backgroundColor: red600 }}
                        title="This is a very cool feature"
                        subtitle="Just using this will blow your mind."
                    />
                    <Slide
                        media={<img src="./8e550c9cc406babdc056ff8098f5ba57.png" />}
                        mediaBackgroundStyle={{ backgroundColor: blue400 }}
                        contentStyle={{ backgroundColor: blue600 }}
                        title="Ever wanted to be popular?"
                        subtitle="Well just mix two colors and your are good to go!"
                    />
                    <Slide
                        media={<img src="./8e550c9cc406babdc056ff8098f5ba57.png" />}
                        mediaBackgroundStyle={{ backgroundColor: green400 }}
                        contentStyle={{ backgroundColor: green600 }}
                        title="May the force be with you"
                        subtitle="The Force is a metaphysical and ubiquitous power in the Star Wars universe."
                    />
                </AutoRotatingCarousel>
            </div>
        );
    }
}