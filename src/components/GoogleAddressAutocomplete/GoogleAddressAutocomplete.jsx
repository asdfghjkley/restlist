/**
 * Ha bisogno dello script google nell'index.html
 * <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={key}"></script>
 */
import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import api from './../../store/api/index';
import { AutoComplete as MuiAutoComplete } from 'material-ui';
import { Field, change } from 'redux-form';
import { TextField, AutoComplete } from 'redux-form-material-ui';

const propTypes = {
    address: PropTypes.shape({
        name: PropTypes.string.isRequired,
        hintText: PropTypes.string.isRequired,
        floatingLabelText: PropTypes.string.isRequired
    }),
    civic: PropTypes.shape({
        name: PropTypes.string.isRequired,
        hintText: PropTypes.string.isRequired,
        floatingLabelText: PropTypes.string.isRequired
    }),
    myForm: PropTypes.string.isRequired,
    disabled: PropTypes.bool
};
const style = {
    floatingLabelStyle: {
        fontSize: '16px'
    },
    textFieldStyle: {
        fontSize: '13px',
        width: '80%',
        textAlign: 'left'
    },
    civicTextFieldStyle: {
        fontSize: '13px',
        width: '20%',
        textAlign: 'right'
    },
    menuItemStyle: {
        fontSize: '12px'
    },
    container: {
        display: 'flex'
    }
};

@connect(
    () => { return {}; },
    { change }
)
class GoogleAddressAutocomplete extends Component {
    static propTypes = propTypes;

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            data: [],
            searchText: '',
            disableCivic: true
        };
        this.updateFromCivic = {
            updateInput: null,
            newRequest: null
        };
    }

    updateDatasource(data) {
        if (!data || !data.length) {
            return false;
        }
        this.setState({
            dataSource: data.map(place => place.description),
            data
        });
    }

    onUpdateInput(event, searchText, prevText, fromCivic) {
        if(!fromCivic) this.resetCivic();
        api.google.autoCompleteCancel();
        api.google.autoComplete(searchText)
            .then(result => this.updateDatasource(result.data.predictions))
            .catch(err => { throw err; });
    }

    onNewRequest() {
        const data = this.state.data;

        api.google.details(data[0].place_id)
            .then(result => {
                const response = result.data.result;
                this.onGoogleResponse(response);
                this.setState({
                    searchText: response.formatted_address,
                    disableCivic: !(_.includes(response.types, 'route') || _.includes(response.types, 'street_address'))
                });
            })
            .catch(err => { throw err; });
    }

    onGoogleResponse(googleResp) {
        const { myForm, change } = this.props;
        for (let ac of googleResp.address_components) {
            change(myForm, ac.types[0] + '_SHORT_NAME', ac.short_name);
            change(myForm, ac.types[0] + '_LONG_NAME', ac.long_name);
        }
        change(myForm, 'place_id', googleResp.place_id);
        change(myForm, 'formatted_address', googleResp.formatted_address);
        change(myForm, 'lat', googleResp.geometry.location.lat);
        change(myForm, 'lng', googleResp.geometry.location.lng);
        change(myForm, 'google_maps_url', googleResp.url);

        if (_.isFunction(this.props.onGoogleResponse)) this.props.onGoogleResponse(googleResp);
    }

    resetCivic() {
        const { myForm, change } = this.props;
        change(myForm, 'street_number_LONG_NAME', '');
        change(myForm, 'street_number_SHORT_NAME', '');
        change(myForm, 'postal_code_LONG_NAME', '');
        change(myForm, 'postal_code_SHORT_NAME', '');
        this.setState({ disableCivic: true });
    }

    componentDidUpdate() {
        if (this.updateFromCivic.updateInput) {
            this.onUpdateInput(null, this.updateFromCivic.updateInput, null, true);
            this.updateFromCivic.newRequest = this.updateFromCivic.updateInput;
            this.updateFromCivic.updateInput = null;
        } else if (this.updateFromCivic.newRequest) {
            this.onNewRequest();
            this.updateFromCivic.newRequest = null;
        }
    }

    render() {
        const { address, civic, disabled } = this.props;

        return (
            <div style={style.container} >
                <Field
                    component={AutoComplete}
                    openOnFocus={true}
                    name={address.name}
                    searchText={this.state.searchText}
                    hintText={address.hintText}
                    filter={MuiAutoComplete.caseInsensitiveFilter}
                    dataSource={this.state.dataSource}
                    floatingLabelText={address.floatingLabelText}
                    floatingLabelStyle={style.floatingLabelStyle}
                    textFieldStyle={style.textFieldStyle}
                    menuProps={{ menuItemStyle: style.menuItemStyle }}
                    disabled={disabled}

                    ref={this.props.getRef}
                    onChange={this.onUpdateInput.bind(this)}
                    onNewRequest={this.onNewRequest.bind(this)}
                />
                <Field
                    name={civic.name}
                    hintText={civic.hintText}
                    floatingLabelText={civic.floatingLabelText}
                    floatingLabelStyle={style.floatingLabelStyle}
                    type={civic.type}
                    style={style.civicTextFieldStyle}
                    component={CivicNumber}
                    disabled={this.state.disableCivic}
                    onChange={(event, value) => {
                        let split_addr = this.state.data[0].description.split(', ');
                        let remove = (split_addr.length == 5) ? 1 : 0;
                        split_addr.splice(1, remove, value);
                        this.updateFromCivic.updateInput = split_addr.join(', ');
                        this.setState({ disableCivic: true });
                    }}
                />
            </div>
        );
    }

}

class CivicNumber extends Component {

    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
        if (this.props.meta.active) {
            document.getElementsByName('street_number_SHORT_NAME')[0].focus();
        }
    }

    render() {
        return <TextField {...this.props} />;
    }
}

export default GoogleAddressAutocomplete;