import React, { Component, PropTypes } from 'react';

const style = {
    container: {
        border: '2px solid black',
        boxShadow: 'rgb(136, 136, 136) 5px 10px 18px',
        borderRadius: '5px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
};

export class Paper extends Component {
    static propTypes = {

    };

    render() {
        return (
            <div style={style.container}>  
                {this.props.children}
            </div>
        );
    }
}