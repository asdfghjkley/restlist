import React, { Component, PropTypes } from 'react';
import { Avatar as MuiAvatar } from 'material-ui';

const style = {
};

export class Avatar extends Component {
    static propTypes = {
        src: PropTypes.string.isRequired,
        style: PropTypes.object,
        size: PropTypes.number
    };

    render() {
        return (
            <MuiAvatar
                src={this.props.src}
                style={{ ...style, ...this.props.style }}
                size={this.props.size}
            />
        );
    }
}