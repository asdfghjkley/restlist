import React, { Component, PropTypes } from 'react';
import { Dialog, FlatButton } from 'material-ui';

const propTypes = {
    onClose: PropTypes.func.isRequired,
    errorMessage: PropTypes.any
};
// const style = {};
export default class ServerErrorMessage extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
    }

    handleClose = () => {
        const { onClose } = this.props;
        onClose();
    }

    mapErrorMessage = () => {
        const { errorMessage } = this.props;
        if (typeof errorMessage === 'undefined') return null;
        if (errorMessage === null) return null;
        if (errorMessage.length <= 0) return null;

        if (errorMessage.constructor !== String) return null; // 'Global Error //da gestire

        return errorMessage;
    }

    getActionButtons = () => {
        return (
            <FlatButton
                label="chiudi"
                primary={true}
                onTouchTap={this.handleClose}
            />
        );
    }

    render() {
        const mappedErrorMessage = this.mapErrorMessage();

        return (
            <Dialog
                actions={this.getActionButtons()}
                modal={false}
                open={mappedErrorMessage !== null}
                onRequestClose={this.handleClose}
            >
                {
                    mappedErrorMessage
                }
            </Dialog>
        );
    }
}