import React, { Component, PropTypes } from 'react';
import './Placeholder.css';

const propTypes = {
    height: PropTypes.number.isRequired,
    ready: PropTypes.bool.isRequired
};

class Placeholder extends Component {
    static propTypes = propTypes;
    render() {
        const { height, ready } = this.props;

        return (
            ready
                ? this.props.children
                : <div className='animated-background' style={{ height: height }} />
        );
    }
}
export default Placeholder;