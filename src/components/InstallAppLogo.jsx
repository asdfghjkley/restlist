import React, { Component, PropTypes } from 'react';
import logoImgApple from '../assets/img/download_app_apple.png';
import logoImgAndroid from '../assets/img/download_app_android.png';

const propTypes = {
    style: PropTypes.object,
    system: PropTypes.string,
    link: PropTypes.string
};
const style = {
    height: '50px',
    width: '160px'
};

export class InstallAppLogo extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
        let { system } = props;

        this.logoImg = undefined;
        switch(system){
            case 'apple':
                this.logoImg = logoImgApple;
                break;
            case 'android':
                this.logoImg = logoImgAndroid;
                break;
            default:
                break;
        }
    }

    render() {
        if(this.logoImg === undefined)
            return (<div />);
        else
            return (
                <a href={this.props.link}>
                <img
                    style={{ ...style, ...this.props.style }}
                    src={this.logoImg}
                />
                </a>
            );
    }
}