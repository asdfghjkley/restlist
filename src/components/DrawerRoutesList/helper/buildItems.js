import React from 'react';
import { ListItem, Subheader, FontIcon } from 'material-ui';

export default (drawerItems, push) => {
    const sections = [];

    for (let [key, value] of Object.entries(drawerItems)) {
        if (value.subHeader) sections.push(<Subheader key={`drawer.subheader.${key}`}>{value.subHeader}</Subheader>);
        sections.push(
            <ListItem
                key={`drawer.listItem.${key}`}
                onTouchTap={() => {
                    if (!value.disabled) {
                        push(value.routePath);
                    }
                }}
                value={value.index}
                disabled={value.disabled}
                primaryText={value.primaryText}
                leftIcon={<FontIcon className="material-icons">{value.leftIcon}</FontIcon>}
            />
        );
    }

    return sections;
};