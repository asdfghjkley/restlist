import React, { Component, PropTypes } from 'react';
import { makeSelectable, List } from 'material-ui';
import buildItems from './helper/buildItems';

export default wrapState(makeSelectable(List));

function wrapState(ComposedComponent) {
    return class DrawerRoutesList extends Component {
        static propTypes = {
            push: PropTypes.func.isRequired,
            defaultIndexSelected: PropTypes.number,
            items: PropTypes.object.isRequired
        }
        constructor(props) {
            super(props);
        }

        render() {
            const { items, push } = this.props;

            return (
                <ComposedComponent
                    value={this.state.selectedIndex}
                    onChange={this.handleRequestChange}
                >
                    {buildItems(items, push)}
                </ComposedComponent>
            );
        }

        componentWillMount() {
            this.setState({
                selectedIndex: this.props.defaultIndexSelected
            });
        }

        handleRequestChange = (event, index) => {
            this.setState({
                selectedIndex: index
            });
        }
    };
}