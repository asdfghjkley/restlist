import React, { Component/*, PropTypes*/ } from 'react';
import { Dialog, FlatButton } from 'material-ui';

import ReactPDF from 'react-pdf';
import pdfPrivacy from 'file-loader!./../../assets/privacy/Italiano.pdf';


const propTypes = {
};
const style = {
    wrapper: {
        textAlign: 'center',
        width: '100%',
        padding: 0,
        maxWidth: '1024px',
        maxHeight: '730px',
        minHeight: document.body.clientHeight
    }
};

class PdfViewer extends Component {
    static propTypes = propTypes;
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            pageIndex: 0,
            pageNumber: 0,
            position: 'relative'
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ open: nextProps.open });
    }


    onDocumentLoad = ({ total }) => {
        this.setState({ total });
    }

    onPageLoad = ({ pageIndex, pageNumber }) => {
        this.setState({ pageIndex, pageNumber });
    }
    render() {
        return (
            <div>
                <Dialog
                    modal={false}
                    open={this.state.open}
                    actions={this.getAction()}
                    autoDetectWindowHeight={true}
                    autoScrollBodyContent={true}
                    contentStyle={style.wrapper}
                    style={{ position: 'relative', padding: 0 }}
                    bodyStyle={{ height: 500, position: 'relative', padding: 0 }}
                    onRequestClose={this.closeDialog}
                >
                    <ReactPDF
                        loading='...'
                        file={pdfPrivacy}
                        pageIndex={this.state.pageIndex}
                        scale={1.5}
                        onDocumentLoad={this.onDocumentLoad}
                        onPageLoad={this.onPageLoad}
                        style={{ zIndex: 0 }}
                    />
                </Dialog>
            </div>
        );
    }
    getAction = () => {
        return (
            <div>
                <FlatButton
                    label={`Page ${this.state.pageNumber} of ${this.state.total} `}
                    primary={false}
                    disabled={true}
                />
                <FlatButton
                    label={'close'}
                    primary={false}
                    onClick={this.closeDialog}
                />
                <FlatButton
                    label={'<'}
                    primary={true}
                    onClick={() => { this.setState({ pageIndex: this.state.pageIndex >= 0 ? this.state.pageIndex - 1 : 0 }); }}
                />
                <FlatButton
                    label={'>'}
                    primary={true}
                    onClick={() => { this.setState({ pageIndex: this.state.pageIndex + 1 }); }}
                />
            </div>
        );
    }
    closeDialog = () => {
        this.setState({ open: false });
        this.props.onClose();
    }
}

export default PdfViewer;