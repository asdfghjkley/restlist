import React, { Component, PropTypes } from 'react';

const style = {
    container: {
        width: '250px',        
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '10px',
    },
    image: {
        width: '250px', 
        boxShadow: 'rgb(136, 136, 136) 0px 0px 18px',
        borderRadius: '5px',
    },
    name: {
        padding: '20px 0',
    },
    texts: {
        alignItems: 'start',
        paddingRight: '10px',
    },
    indicator: {
        paddingLeft: '10px',
    },
};

export class ProductCard extends Component {
    static propTypes = {

    };

    render() {
        return (
            <div style={style.container}>  
                <img src={this.props.image} style={style.image}/>
                <div style={style.name}>HANAMI SOUCHI GOURMET</div>
                <div className="row">
                    <div className="column" style={style.texts}>
                        <div>Giaponesse</div>
                        <div>$25 prezzo medio</div>
                        <div>2.6km - IUgbvd</div>
                    </div>
                    <div style={style.indicator}>
                        <svg width="80" height="80">
                            <circle cx="40" cy="40" r="38" fill="none" stroke="lightgrey" strokeWidth="2"/>
                            <text x="40" y="40" textAnchor="middle" fill="gray" fontSize="12">0.0</text>
                            <text x="40" y="50" textAnchor="middle" fill="gray" fontSize="8">/10</text>
                        </svg>
                    </div>
                </div>
            </div>
        );
    }
}