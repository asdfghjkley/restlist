export const isGuest = ({ user, isAuthenticated }) => {
    if (!user && !isAuthenticated) return true;
    return false;
};

export const isUserInLocalStorage = ({ user, isAuthenticated }) => {
    if (user && !isAuthenticated) return true;
    return false;
};

export const isUser = ({ user, isAuthenticated }) => {
    if (isAuthenticated && user && user.tipo === 'cliente') return true;
    return false;
};

export const isFirm = ({ user, isAuthenticated }) => {
    if (isAuthenticated && user && user.tipo === 'locale') return true;
    return false;
};